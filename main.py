from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from numpy import arange
from PIL.Image import open

width = 600
height = 600

camera_x_rotation = 28
camera_y_rotation = 0
camera_angle_delta = 4
camera_light_attenuation = 0.01
camera_light_cutoff = 20

terrain_size = 8
terrain_delta = 0.5
terrain_texture = 'texture.bmp'

lamps_positions = [(terrain_size - 1, z) for z in arange(-terrain_size, terrain_size + 1, 5)]
lamps_colors = [(1 if i % 3 == 0 else 0, 1 if i % 3 == 1 else 0, 1 if i % 3 == 2 else 0, 1)
                for i in range(len(lamps_positions))]
lamp_attenuation = 0.01
lamp_height = 2

car_matrix = [1, 0, 0, 0,
              0, 1, 0, 0,
              0, 0, 1, 0,
              0, 1.05, 0, 1]
car_movement_speed = 0.2
car_rotation_speed = 5
car_light_color = (1, 1, 1)
car_light_attenuation = 0.01
car_light_cutoff = 30

red = (0.8, 0, 0)
orange = (0.8, 0.5, 0)
yellow  = (1, 1, 0)
gray = (0.6, 0.6, 0.6)
black = (0, 0, 0)

terrain_texture_id = 0

activated_lights = set()


def init():
    global terrain_texture_id

    glClearColor(0, 0.6, 1, 1)
    glEnable(GL_DEPTH_TEST)
    reshape(width, height)

    glEnable(GL_LIGHTING)

    image = open(terrain_texture)
    terrain_texture_id = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, terrain_texture_id)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
    glTexImage2D(GL_TEXTURE_2D, 0, 3, image.size[0], image.size[1], 0, GL_RGB, GL_UNSIGNED_BYTE,
                 image.tobytes('raw', 'RGB', 0, -1))


def update():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    handle_camera()
    draw_terrain()
    draw_lamps()
    draw_car()

    glutSwapBuffers()


def reshape(new_width, new_height):
    global width, height

    width = new_width
    height = new_height
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(60, width / height, 0.1, 100)


def special(key, x, y):
    global camera_x_rotation, camera_y_rotation

    if key == GLUT_KEY_UP:
        camera_x_rotation += camera_angle_delta
    if key == GLUT_KEY_DOWN:
        camera_x_rotation -= camera_angle_delta
    if key == GLUT_KEY_LEFT:
        camera_y_rotation += camera_angle_delta
    if key == GLUT_KEY_RIGHT:
        camera_y_rotation -= camera_angle_delta


def keyboard(key, x, y):
    global car_matrix

    current_matrix = glGetFloat(GL_MODELVIEW_MATRIX)
    glLoadMatrixf(car_matrix)
    if key == b'w':
        glTranslate(car_movement_speed, 0, 0)
    if key == b's':
        glTranslate(-car_movement_speed, 0, 0)
    if key == b'a':
        glTranslate(car_movement_speed, 0, 0)
        glRotate(car_rotation_speed, 0, 1, 0)
    if key == b'd':
        glTranslate(car_movement_speed, 0, 0)
        glRotate(-car_rotation_speed, 0, 1, 0)
    car_matrix = glGetFloat(GL_MODELVIEW_MATRIX)
    glLoadMatrixf(current_matrix)

    try:
        int_key = int(key)
        if int_key not in activated_lights:
            glEnable(eval('GL_LIGHT' + str(int_key)))
            activated_lights.add(int_key)
        else:
            glDisable(eval('GL_LIGHT' + str(int_key)))
            activated_lights.remove(int_key)
    except (ValueError, NameError):
        pass


def handle_camera():
    glLight(GL_LIGHT0, GL_POSITION, (0, 0, 0, 1))
    glLight(GL_LIGHT0, GL_DIFFUSE, (1, 1, 1, 1))
    glLight(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, camera_light_attenuation)
    glLight(GL_LIGHT0, GL_SPOT_DIRECTION, (0, 0, -1))
    glLight(GL_LIGHT0, GL_SPOT_CUTOFF, camera_light_cutoff)

    glTranslate(0, 0, -10)
    glRotate(camera_x_rotation, 1, 0, 0)
    glRotate(camera_y_rotation, 0, 1, 0)


def draw_terrain():
    glMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE, (1, 1, 1))
    glEnable(GL_TEXTURE_2D)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)
    glBindTexture(GL_TEXTURE_2D, terrain_texture_id)
    glBegin(GL_QUADS)
    for x in arange(-terrain_size, terrain_size, terrain_delta):
        for z in arange(-terrain_size, terrain_size, terrain_delta):
            u1 = (x + terrain_size) / (terrain_size + terrain_size)
            u2 = (x + terrain_delta + terrain_size) / (terrain_size + terrain_size)
            v1 = (z + terrain_size) / (terrain_size + terrain_size)
            v2 = (z + terrain_delta + terrain_size) / (terrain_size + terrain_size)
            glTexCoord(u1, v1)
            glNormal(0, 1, 0)
            glVertex(x, 0, z)
            glTexCoord(u2, v1)
            glNormal(0, 1, 0)
            glVertex(x + terrain_delta, 0, z)
            glTexCoord(u2, v2)
            glNormal(0, 1, 0)
            glVertex(x + terrain_delta, 0, z + terrain_delta)
            glTexCoord(u1, v2)
            glNormal(0, 1, 0)
            glVertex(x, 0, z + terrain_delta)
    glEnd()
    glDisable(GL_TEXTURE_2D)


def draw_lamps():
    for i in range(len(lamps_positions)):
        x, z = lamps_positions[i]
        glPushMatrix()
        glTranslate(x, 0, z)
        glRotate(-90, 1, 0, 0)
        glMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE, gray)
        glutSolidCylinder(0.1, lamp_height, 10, 10)
        glTranslate(0, 0, lamp_height + 0.5)
        glMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE, yellow)
        glutWireSphere(0.5, 10, 10)
        current_light = eval('GL_LIGHT' + str(i + 3))
        glLight(current_light, GL_POSITION, (0, 0, 0, 1))
        glLight(current_light, GL_DIFFUSE, lamps_colors[i])
        glLight(current_light, GL_CONSTANT_ATTENUATION, 0)
        glLight(current_light, GL_QUADRATIC_ATTENUATION, lamp_attenuation)
        glPopMatrix()


def draw_car():
    glPushMatrix()
    glMultMatrixf(car_matrix)
    glPushMatrix()
    glTranslate(-1, 0, 0)
    glScale(3, 1.5, 1)
    glMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE, red)
    glutSolidCube(1)
    glPopMatrix()
    glPushMatrix()
    glTranslate(1, -0.25, 0)
    glMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE, orange)
    glutSolidCube(1)
    glPopMatrix()
    glPushMatrix()
    glTranslate(1, -0.75, 0.5)
    glMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE, black)
    glutSolidTorus(0.1, 0.2, 10, 10)
    glTranslate(0, 0, -1)
    glutSolidTorus(0.1, 0.2, 10, 10)
    glTranslate(-2.5, 0, 0)
    glutSolidTorus(0.1, 0.2, 10, 10)
    glTranslate(0, 0, 1)
    glutSolidTorus(0.1, 0.2, 10, 10)
    glPopMatrix()
    glTranslate(1.5, -0.5, 0.3)
    glMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE, yellow)
    glutWireSphere(0.1, 10, 10)
    glLight(GL_LIGHT1, GL_POSITION, (0, 0, 0, 1))
    glLight(GL_LIGHT1, GL_DIFFUSE, car_light_color)
    glLight(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 0)
    glLight(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, car_light_attenuation)
    glLight(GL_LIGHT1, GL_SPOT_DIRECTION, (1, 0, 0))
    glLight(GL_LIGHT1, GL_SPOT_CUTOFF, car_light_cutoff)
    glTranslate(0, 0, -0.6)
    glutWireSphere(0.1, 10, 10)
    glLight(GL_LIGHT2, GL_POSITION, (0, 0, 0, 1))
    glLight(GL_LIGHT2, GL_DIFFUSE, car_light_color)
    glLight(GL_LIGHT2, GL_CONSTANT_ATTENUATION, 0)
    glLight(GL_LIGHT2, GL_QUADRATIC_ATTENUATION, car_light_attenuation)
    glLight(GL_LIGHT2, GL_SPOT_DIRECTION, (1, 0, 0))
    glLight(GL_LIGHT2, GL_SPOT_CUTOFF, car_light_cutoff)
    glPopMatrix()


def main():
    glutInit()
    glutInitWindowSize(width, height)
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH)
    glutCreateWindow('OpenGL')
    glutIdleFunc(update)
    glutDisplayFunc(update)
    glutReshapeFunc(reshape)
    glutSpecialFunc(special)
    glutKeyboardFunc(keyboard)
    init()
    glutMainLoop()


if __name__ == '__main__':
    main()
